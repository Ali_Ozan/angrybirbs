using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [SerializeField] float _launchForce = 400f;
    [SerializeField] float _maxDragDistance = 2.5f;

    Vector2 _startPosition;
    Rigidbody2D _rigidBody2D;
    SpriteRenderer _spriteRenderer;
    LineRenderer _lineRenderer;

    private void Awake()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _lineRenderer = GetComponent<LineRenderer>();
    }

    void Start()
    {
        _startPosition = _rigidBody2D.position;
        _lineRenderer.SetPosition(0, _startPosition);//buradaki s�f�r line rendererdaki index. orada 0 ve 1  indexli 2 nokta var onlar�n aras�nda line �izilecek
        _lineRenderer.enabled = false;
        _rigidBody2D.isKinematic = true;
    }

    void OnMouseDown()
    {
        _spriteRenderer.color = Color.red;       
    }

    void OnMouseUp()
    {
        Vector2 currentPosition = _rigidBody2D.position;
        Vector2 direction = _startPosition - currentPosition;

        _rigidBody2D.isKinematic = false;
        _rigidBody2D.AddForce(direction * _launchForce);

        _spriteRenderer.color = Color.white;
        _lineRenderer.enabled = false;
    }

    void OnMouseDrag()
    {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 desiredPosition = mousePosition;

        float distance = Vector2.Distance(desiredPosition, _startPosition);
        if (distance > _maxDragDistance)
        {
            Vector2 direction = desiredPosition - _startPosition;
            direction.Normalize();
            desiredPosition = _startPosition + (direction * _maxDragDistance);
        }

        if (desiredPosition.x > _startPosition.x)
            desiredPosition.x = _startPosition.x;

        _rigidBody2D.position = desiredPosition;
        _lineRenderer.SetPosition(1, desiredPosition);
        _lineRenderer.enabled = true;
    }


    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        int colliderLayerIndex = collision.collider.gameObject.layer;
        Debug.Log(colliderLayerIndex.ToString());
        if (colliderLayerIndex == 7 || colliderLayerIndex == 8 || colliderLayerIndex == 9) //ground, crates or monsters
            StartCoroutine(ResetAfterDelay());
    }

    IEnumerator ResetAfterDelay()
    {
        yield return new WaitForSeconds(3);
        _rigidBody2D.position = _startPosition;
        _rigidBody2D.SetRotation(0);
        _rigidBody2D.angularVelocity = 0;
        _rigidBody2D.isKinematic = true;
        _rigidBody2D.velocity = Vector2.zero;

    }
}
