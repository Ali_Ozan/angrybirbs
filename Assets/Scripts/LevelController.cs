using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    [SerializeField] string _nextLevelName;

    Monster[] _monsters;
    
    void OnEnable()
    {
        _monsters = FindObjectsOfType<Monster>();
    }
    void Update()
    {
        if (MonstersAreAllDead())
            GoToNextLevel();
    }

    void GoToNextLevel()
    {
        SceneManager.LoadScene(_nextLevelName);
    }

    bool MonstersAreAllDead()
    {
        foreach (var Monster in _monsters)
        {
            if (Monster.gameObject.activeSelf)
                return false;
        }

        return true;
    }
}
