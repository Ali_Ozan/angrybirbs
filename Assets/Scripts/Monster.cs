using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase] //bunu ekledi�im i�in hierarchy'de monster'a t�klad���mda particle system ya da herhangi ba�ka bir child de�il, selection base'i ta��yan componenti yani monster�n kendisini se�ecek.

public class Monster : MonoBehaviour
{
    [SerializeField] Sprite _deadSprite;
    [SerializeField] ParticleSystem _particleSystem;

    bool _hasDied = false;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (ShouldDieFromCollision(collision))
        {
            StartCoroutine(Die());
        }            
    }

    bool ShouldDieFromCollision(Collision2D collision)
    {
        if (_hasDied) //What is dead may never die...
            return false;
        Bird bird = collision.gameObject.GetComponent<Bird>();
        if (bird != null)
            return true;

        if (collision.contacts[0].normal.y < -0.5f)
            return true;

        return false;
    }

    IEnumerator Die()
    {
        _hasDied = true;
        GetComponent<SpriteRenderer>().sprite = _deadSprite;
        _particleSystem.Play();
        yield return new WaitForSeconds(2);
        gameObject.SetActive(false);
    }
}
